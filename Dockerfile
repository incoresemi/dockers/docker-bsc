# InCore Development Environment Image
FROM ubuntu:20.04

# UPDATE / UPGRADE

ENV PATH=$PATH:/bluespec/bin:/veril/bin

# This step ensures that the docker image is fully up to date and is ready for dependency installation.
RUN apt update

# ALL DEPENDENCIES
# This step installs all of the dependencies needed to build the tooling for bluespec and python3.
RUN DEBIAN_FRONTEND=noninteractive apt install -y --no-install-recommends ghc libghc-regex-compat-dev libghc-syb-dev \
  libghc-old-time-dev libghc-split-dev tcl-dev build-essential pkg-config autoconf gperf \
  g++ gcc automake gawk python3 python3-pip flex bison tcl-itcl4-dev tk-itk4-dev make \
  ccache tcl-dev iverilog libfl2 libfl-dev zlib1g zlib1g-dev numactl perl-doc perl \
  python-is-python3 software-properties-common xxd

RUN add-apt-repository ppa:git-core/ppa -y
RUN apt update
RUN DEBIAN_FRONTEND=noninteractive apt install -y --no-install-recommends git

# INSTALL BLUESPEC (BSC)
# This step installs the bluespec compiler, which takes the code that describes the core in .bsv files and turns it into Verilog and an executable.
RUN git clone --recursive https://github.com/B-Lang-org/bsc && \
    cd bsc && \
    mkdir /bluespec && \
    git checkout 2021.12.1 && \
	  make PREFIX=/bluespec/ -j8 install-src 1>/dev/null && \
  	cd .. && \
	  rm -rf bsc 


RUN git clone https://github.com/verilator/verilator && \
    cd verilator && \
    mkdir /veril && \
    git checkout v4.106 && \
    autoconf && \
    ./configure --prefix=/veril/ 1>/dev/null && \
    make -j8 && \
    make install && \
    cd .. && \
    rm -rf verilator

RUN ln -fs /usr/bin/python3 /usr/bin/python & \
    ln -fs /usr/bin/pip3 /usr/bin/pip

RUN mkdir /home/workdir/
WORKDIR /home/workdir/
